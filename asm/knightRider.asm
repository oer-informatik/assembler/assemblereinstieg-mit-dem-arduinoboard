.include "m328Pdef.inc"
;----------------------------------------------------------------------------
;* Titel            :Beispielprogramm
;* Prozessor        :ATmega8
;* Schaltung        :Port D.2 bis Port D.4 an LED 1 bis 3
;* Datum            :07.03.2018
;* Autor            :Hannes Stein
;----------------------------------------------------------------------------
;.include "m8def.inc"
;----------------------------------------------------------------------------
; Reset and Interruptvectoren    ; VNr. Beschreibung
begin:    rjmp    main    ;1  Power on Reset (siehe S43)
    reti        ;2  Int0-Interrupt (siehe S43)
    reti        ;3  Int1-Interrupt
    reti        ;4  TC2 Compare Match
    reti        ;5  TC2 Overflow
    reti        ;6  TC1 Capture
    reti        ;7  TC1 Compare Match A
    reti        ;8  TC1 Compare Match B
    reti        ;9  TC1 Overflow
    reti        ;10 TC0 Overflow
    reti        ;11 SPI, STC Serial Transfer Complete
    reti        ;12 UART Rx Complete
    reti        ;13 UART Data Register Empty
    reti        ;14 UART Tx Complete
    reti        ;15 ADC Conversion Complete
    reti        ;16 EEPROM Ready
    reti        ;17 Analog Comparator
    reti        ;18 TWI (I2C) Serial interface
    reti        ;19 Store Program Memory Redy
;----------------------------------------------------------------------------
;Start, Power ON, reset  (Initialisierungsroutine)

;Start, Power ON, reset  (Initialisierungsroutine)
main:    ldi    r16, lo8(RAMEND)   ; Lade in r16 die Adresse vom RAMEnde (untere 8 bit) (siehe S43)
    out    SPL,r16    ; Init Stackpointer LO (siehe S43)
    ldi    r16,hi8(RAMEND)    ; Lade in r16 die Adresse vom RAMEnde (obere 8 bit)
    out    SPH,r16    ; Init Stackpointer HI
    ldi    r16,0b11111111    ; Setze alle bits von r16 auf 1
    out    DDRD,r16    ; kopiere den Inhalt von zu DataDiRectionD
; (Setze die Pins fuer Port D auf Output)
    ldi    r16,0b00000000    ; Setze alle bits von r16 auf 0
    out    PORTD,r16    ; Schreibe den Inhalt von r16 nach Port D (Alle pins auf LOW)
    ldi    r17,0b00000000    ; Setze alle bits von r17 auf 0 (Init Zaehlervariable1)
    ldi    r18,0b00000001    ; Setze alle bits von r18 auf 0 (Init Zaehlervariable2)
;----------------------------------------------------------------------------
;        Hauptschleife
mainloop:    wdr        ; watchdog reset (siehe S43)
    inc    r16    ; erhoehe r16 um 1 (siehe S61)
    brne    skip    ; falls r16    nicht gleich 0 springe zu skip (siehe S42, S61)
; (erst bei Ueberlauf von r16=257 geht es weiter)
    inc    r17    ; erhoehe r17 um 1
    brne    skip    ; falls r17    nicht gleich 0 springe zu skip
; (erst bei Ueberlauf von r17=257 geht es weiter)
    rcall    up1    ; Unterprogrammaufruf, relativ: springe zu up1 (siehe S42, S74)

skip:    rjmp    mainloop    ; sprung (ohne Bedingung) zu mainloop
;----------------------------------------------------------------------------
;        Unterprogramm
up1:    rol    r18    ; Bit-Rotation im r18 (rotate left, throw Cary)
    out    PORTD,r18    ; Ausgabe r18 an PORTD
    ret        ; Ruecksprung aus Unterprogramm
;----------------------------------------------------------------------------
