@echo off
 
echo Assemblieren
avr-as -g -mmcu=atmega328p -o %1.o %1.asm
 
echo Linken
avr-ld -o %1.elf %1.o
 
echo Intel-Hex erstellen
avr-objcopy -O ihex -R .eeprom %1.elf %1.hex
 
echo Upload
avrdude -C "C:\Program Files (x86)\Arduino\hardware\tools\avr\etc\avrdude.conf" -p atmega328p -c arduino -P com%2 -b 115200 -D -U flash:w:%1.hex:i
 
echo Fertig
pause