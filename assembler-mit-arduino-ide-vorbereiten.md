## Arduino-IDE zur Programmierung von Assembler vorbereiten

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/assembler-mit-arduino-ide-vorbereiten</span>

> **tl/dr;** _(ca. 5 min Lesezeit): Kann man mit Arduino-Bordmitteln Assembler programmieren? Zugegeben: es ist Schwachsinn: da geben sich die Leute von Arduino viel Mühe, um die ganze Komplexität der Microcontrollerprogrammierung zu verstecken, und dann versuchen wir, das alles wieder zurückzudrehen. Teil 2: Vorbereitung (zu [Teil 2, der eigentlichen Programmierung, geht es hier](https://oer-informatik.de/assembler-mit-arduino-ide-programmieren))._

### Ziel dieses Artikels:

Mithilfe der Programme, die in der Arduino-IDE enthalten sind, soll ein Assembler-Programm auf einen AVR-ATMega-Microcontroller kompiliert und geladen werden. Das passiert in vier Schritten:

* Zunächst wird geprüft, ob die Installation der Arduino-IDE erfolgreich war und ob das Board auf regulärem Weg ansprechbar ist.

* Dann werden die Pfadangaben der benötigten Tools aus der Arduino-IDE im System gesetzt, damit die Bedienung später komfortabler ist.

* Schließlich muss herausgefunden werden, mit welchem Anschluss der Arduino verbunden ist.

* Um am Ende mit vier Konsolen-Befehlen den Quelltext umzuwandeln und abschließend auf den Arduino zu laden. Das passiert im zweiten Teil dieses Tutorials.

### Die Arduino-Infrastruktur

Der Arduino wurde ursprünglich an einer italienischen Kunsthochschule entwickelt, um Künstler*innen, Designer*innen, Student*innen und Schüler*innen einen einfachen Einstieg in die Welt der Microcontroller zu ermöglichen. Daher versteckt das Arduino-Programm sehr viele technische Details und Prozesse, die unter der Oberfläche passieren, vor Benutzer*innen.

Wir wollen das genaue Gegenteil tun: möglichst viel von dem verstehen, was in Microcontrollern unter der Oberfläche passiert. Zum Einstieg bieten sich kleine, einfache Microcontroller wie ein _PIC_ oder ein _ATMega_ von AVM an. Letzterer steckt in den verbreiteten Arduino-Uno-Boards. Wir nutzen die Tatsache, dass Arduinos weit verbreitet sind, führen jedoch zunächst die eigentliche Idee des Arduinos ad absurdum und suchen nach all den versteckten Hintergrundprozessen.

### Vorbereitungen: nötige Software und Hardware

#### Benötigte Hardware

Für die ersten Schritte sind nur zwei Dinge nötig:

* einen Arduino Uno (möglichst Revision 3 mit dem 8-Bit ATMega328P-Microcontroller)

* ein USB-Kabel für den Arduino

Damit es _später_ interessanter wird, wenn man dann das Board programmieren will, sollte für weitergehende Experimente noch zur Hand sein (wird aber hierfür zunächst nicht gebraucht):

* ein Breadboard
ein Steckbrett, um ohne zu löten Prototypen aufbauen zu können

* ein paar LEDs (z.B. je 5x rot, gelb, grün, blau, weiß),

* ein paar Widerstände (je 20x 330Ohm und 10kOhm könnten reichen)

* ein paar breadboard-freundliche Taster

* zwei Potenziometer (1kOhm oder 10kOhm).

### Die Entwicklungsumgebung (Arduino IDE) installieren

Softwareseitig benötigen wir nur die Arduino-Entwicklungsumgebung. Eine integrierte Entwicklungsumgebung  (_Integrated Development Environment_ - IDE) besteht aus mehreren Komponenten: neben dem Texteditor sind dies bei der Arduino-IDE auch die Tools, die wir benötigen, um den Arduino per USB zu programmieren.

Die jeweils aktuelle und passende Version findet sich hier:
 [https://www.arduino.cc/en/Main/Software](https://www.arduino.cc/en/Main/Software)

Hinweis: Einige günstige Arduinos aus China nutzen einen USB-Treiber, der nicht Bestandteil der gängigen Betriebssysteme ist (CH340G bzw. CH341SER). Sofern dies zutrifft muss noch der jeweilige Treiber installiert werden:

* [Installationsanleitung](https://www.arduined.eu/ch340-windows-8-driver-download/)

* [Orginalquelle Windows CH340G-Treiber](http://www.wch.cn/download/CH341SER_ZIP.html)

* [Orginalquelle MacOSX CH340G-Treiber](http://www.wch.cn/download/CH341SER_MAC_ZIP.html)

* [Orginalquelle Linux CH340G-Treiber](https://github.com/juliagoda/CH341SER)

Nicht erschrecken - die Seiten sind auf Chinesisch...


#### Arduino und Programmierumgebung testen

Wer sich sicher ist, dass der Arduino und die Installation bereits funktioniert, der kann diesen Absatz überspringen.

Mit der oben genannten Hardware und Software sind wir einsatzbereit. Um im späteren Verlauf auszuschließen, dass Installation oder Board nicht funktionieren testen wir den Arduino zunächst einmal in klassischer Art und Weise:

Wenn die Arduino-IDE gestartet ist und der Arduino angeschlossen ist, muss im Menü "Werkzeuge" unter "Board" der _Arduino Uno_ ausgewählt sein, unter "Port" muss ein COM-Port angezeigt werden (ggf. steht dahinter auch "Arduino Uno").

**Die Nummer des COM-Ports wird später noch wichtig, also diese bitte merken!**

 Sollte kein Port erscheinen, muss möglicherweise der CHG-340-Treiber (siehe oben) nachinstalliert werden.

![Arduino-IDE Menü "Werkzeuge"/Port](images/ArduinoIDE-Test-00.webp)

Um zu testen, ob die Kommunikation klappt und ob der Arduino o.k. ist, kann ein Testprogramm geladen werden.
Die Arduino-IDE bringt ein solches Programm direkt mit. Es lässt sich unter `Datei/Beispiele/01. Basic/ Blink` öffnen:

![Dialog Datei/Beispiele/01. Basic/ Blink](images/ArduinoIDE-Test-02.webp)

Wenn die obigen Einstellungen korrekt waren, kann das Beispielprogramm über den Button mit "Pfeil nach rechts"-Symbol (Foto unten, Nr.1) auf den Arduino geladen werden. Im unteren Bereich erscheinen allerlei Meldungen (Nr. 2), bis am Ende im unteren grünen Bereich "Hochladen abgeschlossen" (Nr. 3) erscheint.

![Button zum Laden des Programms und Erfolgsmeldung](images/ArduinoIDE-Test-03.webp)

Wenn das Board nach kurzem flackern regelmäßig blinkt: Glückwunsch. Die Entwicklungsumgebung und der Arduino laufen jetzt, Test bestanden.

#### Die AVR-Programme in der Arduino-IDE finden

Die eigentliche Arduino-IDE ist eine vereinfachte Entwicklungsumgebung für Programme mit der Programmiersprache `C` - die wollen wir (noch) nicht nutzen.

Wir verwenden zunächst aber eine Reihe von Tools, die die Arduino-IDE mitbringt. Diese finden sich in einem Unterordner der Installation:

Arduino-Programm-Ordner, dann im Pfad:  `hardware\tools\avr\bin`

Suchen Sie den Ordner, in dem die Programme `avr-as.exe`, `avrdude.exe`, `avr-ld.exe` und `avr-objcopy.exe` liegen. Der genaue Pfad ist abhängig von der Installationsart - in meinem Windows-System finden die Dateien sich beispielsweise unter `C:\Program Files (x86)\Arduino\hardware\tools\avr\bin`:

![Die Dateien unter `C:\Program Files (x86)\Arduino\hardware\tools\avr\bin`](images/avr-programme.webp)


#### Eine Abkürzung: Zu faul, immer den Pfad zu schreiben? Umgebungsvariablen nutzen!

Wir müssen später die oben gefundenen Programme in der Konsole (PowerShell) mit vollem Pfad aufrufen.

Bei jedem Programmstart müssten wir den gesamten Pfad zu den Dateien angeben, z.B.:

`C:\Program Files (x86)\Arduino\hardware\tools\avr\bin\avrdude`

Das ist umständlich. Deshalb sagen wir dem System, wo diese Programme liegen, damit wir sie zukünftig nur noch mit dem Namen ansprechen können.

Hierzu muss (in Windows) die Funktion "Umgebungsvariablen für dieses Konto bearbeiten" aufgerufen werden (Windows-Taste, dann "Umgeb..." tippen, auf Ergebnis klicken)

![Windows Einstellung für Umgebungsvariablen öffnen](images/Umgebungsvariablen01.webp)

Im erscheinenden Fenster oben "Path" auswählen und "Bearbeiten" anklicken.

![Im Fenster für die Umgebungsvariable "Path" "Bearbeiten" wählen](images/Umgebungsvariablen02.webp)

Einen neuen Path hinzufügen - mit dem Pfad des "/avr/bin"-Ordners, der in der Arduino-Entwicklungsumgebung enthalten ist.

In meinem Fall habe ich eingegeben: `C:\Program Files (x86)\Arduino\/hardware/tools/avr/bin/`

![Einen neuen Path der Umgebungsvariablen anfügen](images/Umgebungsvariablen03.webp)

### Jetzt geht es erst richtig los...

Bislang ist ja noch nicht viel passiert - aber alle Vorbereitungen sind getroffen. Dann kann es jetzt richtig losgehen mit der Assembler-Programmierung in [Teil 2 dieser Artikelserie](https://oer-informatik.de/assembler-mit-arduino-ide-programmieren).

### Links und weitere Informationen

- [Quelle des Programmcodes](https://www.avrfreaks.net/forum/assembly-code-invalid-atmel-studio-60)

- [Quelle des Arbeitsablaufs](https://www.cypherpunk.at/2014/09/native-assembler-programming-on-arduino/)

- [Assembler-Programm in Anlehnung an Edgar Bonet ](https://arduino.stackexchange.com/questions/23146/make-led-blink-using-avr-assembly )

- [Quelle der angepassten m328Pdef.inc](https://raw.githubusercontent.com/DarkSector/AVR/master/asm/include/m328Pdef.inc): .equ wurde durch .set ersetzt, "=" durch "," und ein paar Zeilen auskommentiert, damit es genutzt werden kann

